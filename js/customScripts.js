$(document).ready(function() {

    // TROCA A COR DA NAVBAR QUANDO DESCE UM POUCO
    $(window).scroll(function() {
        var posAtual = $(window).scrollTop(),
            navbar = $('.navbar-transparent');
        if (posAtual > 150) {
            navbar.addClass('navbar-alt');
        } else {
            navbar.removeClass('navbar-alt');
        }
    });

    // TODOS OS BOTOES COM A CLASSE SIDEBAR-TOGGLE ABREM A SIDEBAR, ISSO CUIDA DISSO
    $(".sidebar-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $(".navbar-fixed-top").toggleClass("navbar-sidebar-toggled");
    });

    // TODOS OS BOTOES COM A CLASSE SIDEBAR-TOGGLE-REDIRECT FECHAM A SIDEBAR E DEPOIS REDIRECIONAM, ISSO CUIDA DISSO
    $(".sidebar-toggle-redirect").click(function(e) {
        e.preventDefault();
        $("#wrapper").removeClass("toggled");
        $(".navbar-fixed-top").removeClass("navbar-sidebar-toggled");
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 400);
                target.focus(); // Setting focus
                if (target.is(":focus")) { // Checking if the target was focused
                    return false;
                } else {
                    target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                    target.focus(); // Setting focus
                };
                return false;
            }
        }
    });

    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 400);
                    target.focus(); // Setting focus
                    if (target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        target.focus(); // Setting focus
                    };
                    return false;
                }
            }
        });
    });
});
